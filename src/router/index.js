import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import ProjectCard from "../views/ProjectCard.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: '/education',
    name: 'Education',
    meta: {
      title : "Education"
    },
    component: () =>
      import('../views/Education.vue')
  },
  {
    path: '/career',
    name: 'Career',
    meta: {
      title : "Career"
    },
    component: () =>
      import('../views/Career.vue')
  },
  {
    path: '/resume',
    name: 'CV',
    meta: {
      title : "CV"
    },
    component: () =>
      import('../views/Resume.vue')
  },
  {
    path: '/projects',
    name: 'Projects',
    meta: {
      title : "Projects"
    },
    component: () =>
      import('../views/ProjectsContainer.vue')
  },
  { path: "/project/:id",
    name: "project",
    meta: {
      title : "Projects"
    },
    component: ProjectCard 
  }
];

const router = new VueRouter({
  scrollBehavior() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 100)
    })
  },
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((toRoute, fromRoute, next) => {
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
  window.document.title = toRoute.meta && toRoute.meta.title ? 'Roman Terekhov 👋 - ' + toRoute.meta.title : 'Roman Terekhov 👋';

  next();
})

export default router;