import NavBar from './NavBar.vue'
import AppMain from './AppMain.vue'
import Footer from './Footer.vue'

export {
  NavBar,
  AppMain,
  Footer
}