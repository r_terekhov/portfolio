import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    projects: [
      {
        id: 0,
        title: "Mechanic",
        subtitle: " - сервис по поиску механиков для экстренного ремонта в США",
        whatWeDo: "Разработали мвп сервиса (с логикой, аналогичной Яндекс-такси, но с другими участниками), которое включает в себя клиентское pwa-приложение с возможностью оставить заявку, админ-панель менджера, в которой можно связаться с механиком и выдать ему заказ. Создан telegram-бот, который уведомляет менеджеров при поступлении новой заявки на ремонт. Проект подразумевался как стартап, но из-за того, что мы не сошлись в условиях работы с владельцами идеи проект прекратился:(",
        businessValue: "Ремонт авто достаточно популярен и высоко оплачивается в США, исходя из этого возникла идея создания сервиса, которая позволит найти механикам работу, а клиентам получить помощь.",
        date: "20.03.2021",
        imgSrc: "mechanic.png",
        technologies: "Spring Boot, Mongodb, Firebase, Vue, GCP, NGINX, Docker, Telegarm Bot API",
        links: [
          {
            textLabel: "Frontend(Client app)",
            url: "https://gitlab.com/r_terekhov/mechanic-frontend"
          },
          {
            textLabel: "Frontend(Manager app)",
            url: "https://gitlab.com/r_terekhov/mechanic-frontend-admin"
          },
          {
            textLabel: "Backend",
            url: "https://gitlab.com/r_terekhov/mechanic-backend"
          }
        ]
        
      },
      {
        id: 1,
        title: "IKEA",
        subtitle: " - digital menu.",
        whatWeDo: "Разработали онлайн - меню для сети ресторанов IKEA.",
        businessValue: "Веб-сервис, позволяет определиться с выбором блюд посетителям ресторана и увеличить пропускную способность ресторана IKEA",
        date: "20.03.2020",
        imgSrc: "ikea.png",
        technologies: "Spring Boot, Mongodb, React, GCP, NGINX, Docker",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/ikea.pdf?alt=media",
        links: [
          {
            textLabel: "Frontend",
            url: "https://gitlab.com/r_terekhov/IKEA-menu"
          },
          {
            textLabel: "Backend",
            url: "https://gitlab.com/r_terekhov/ikea-restaurant-menu-backend"
          }
        ]
        
      },
      {
        id: 2,
        title: "Цифровой прорыв 2020",
        subtitle: " - крупнейший IT - марафон в России",
        whatWeDo: `Наш кейс на хактоне был посвящён паркам. Основная проблема парков в России в том, что они непривлекательны для посетителей. Они выбирают торговые центры, кинотеатры и другие развлечения походам в парк. <br> <br>

        В своём решении мы сфокусировались на задаче показать людям красоту парков в их текущем состоянии и привлечь их внимание к ним посредством создания Лиса Каприза - милого зверька, верного помощника, который расскажет обо всех интересных местах в парке.  <br> <br>

        Наша команда разработала приложение, стимулирующее людей чаще ходить в парки и видеть красоту в них. Уникальность решения заключается в том, что доступ ко всему функционалу реализуется через милого и полезного тамагочи - лиса Каприза, который делает процесс посещения парков интерактивным и интересным. Он может провести тебя по парку, предложить тебе персональные рекомендации, рассказать об акциях и мероприятиях, проводимых в парке, поделиться скидками и многое другое.`,

        businessValue: "Бизнесу стало проще соблюдать предписанные государством меры по защите населения в период пандемии. А малый и средний бизнес получает функционал электронных очередей с свои заведения без сложных интеграций и установки дорогостоящих терминалов.",
        date: "27.09.2020",
        imgSrc: "digital-breakthrough-2020-2.png",
        technologies: "Spring Boot, Mongodb, React, NGINX, Docker",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/parkoved.pdf?alt=media", 
      },

      {
        id: 3,
        title: "AMC.Make",
        subtitle: " - американский хакатон, посвщённый урбанистике.",
        whatWeDo: "Представили платформу, информирующую о многолюдности того или иного заведения в режиме реального времени с функционалом электронной очереди, которая может использована в любых заведениях. Данная платформа способствует снижению риска заражения короновирусом за счёт уменьшения скопления людей.",
        businessValue: "Бизнесу стало проще соблюдать предписанные государством меры по защите населения в период пандемии. А малый и средний бизнес получает функционал электронных очередей с свои заведения без сложных интеграций и установки дорогостоящих терминалов.",
        date: "06.09.2020",
        imgSrc: "amc2020.png",
        technologies: "Spring Boot, Mongodb, React, NGINX, Docker",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/amc2020.pdf?alt=media", 
      },

      {
        id: 4,
        title: "Parflab.store",
        subtitle: " - интернет-магазин духов на распив.",
        whatWeDo: "Разработали уникальный дизайн и выполнили разработку интернет - магазина прекрасных духов.",
        businessValue: "У клиента появился удобный инструмент, который позволил ему разгрузить Direct в Instagram. Интернет магазин облегчил работу с клиентами в условиях роста числа заказов.",
        date: "20.07.2020",
        imgSrc: "parflab.png",
        technologies: "Wordpress, Woocommerce",
      },

      {
        id: 5,
        title: "Samoisolation",
        subtitle: " - первый в интернете сервис для самоизоляции.",
        whatWeDo: `Веб-сервис, который был создан в период пандемии короновируса, призванный разнообразить досуг самоизолирующихся людей. <br> <br>

        Делитесь с другими тем, что для вас важно. Ищите вдохновение в занятиях и историях других пользователей.Наслаждайтесь уютной атмосферой общения. Находите помощь и поддержку от людей, попавших в ту же ситуацию, что и вы.`,
        date: "20.07.2020",
        imgSrc: "samoisolation.png",
        technologies: "Spring Boot, Mongodb, React, GCP, NGINX, Docker",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/SMBA_AR_maski.pdf?alt=media",
        links: [
          {
            textLabel: "Frontend",
            url: "https://gitlab.com/r_terekhov/samoisolation-frontend"
          },
          {
            textLabel: "Backend",
            url: "https://gitlab.com/r_terekhov/samoisolation-backend"
          }
        ]
      },

      {
        id: 6,
        title: "Food AR",
        subtitle: "  - еда в дополненной реальности.",
        whatWeDo: `Сервис, который позволяет посетителям заведений с помощью смартфона посмотреть, как будет выглядеть блюдо и определиться с заказом. В сервисе используется технология дополненной реальности AR.`,
        businessValue: "Повышается лояльность посетителей. Возможность посмотреть блюдо до его подачи вызывает огромный интерес посетителей и спасёт от неоправданных ожиданий.",
        date: "20.03.2019",
        imgSrc: "foodar.png",
        technologies: "JS, React, NGINX",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/FUDAR.pdf?alt=media",
        links: [
          {
            textLabel: "Frontend",
            url: "https://gitlab.com/r_terekhov/food-ar"
          }
        ]
      },

      {
        id: 7,
        title: "Navigation Leroy Merlin",
        subtitle: " - кейс сервиса навигации в магазине.",
        whatWeDo: `Многие из вас часами блуждают в гипермаркете в поисках нужного товара? Сервис навгиции позволяет построить кратчайшие маршруты до интересующих позиций, тем самым поход в магазин станет более комфортным и экономичным по времени.`,
        businessValue: "Бизнесу важна лояльность посетителей и хорошие отзывы. Также сервис может помочь штату новых сотрудников магазина быстрее запомнить местаположение товаров и оказывать посетителям помощь в поиске товара выйдет на совершенно иной уровень.",
        date: "20.04.2019",
        imgSrc: "leroy-merlin.png",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/leroy-merlin-navigation.pdf?alt=media"
      },

      {
        id: 8,
        title: "Цвет Настроения",
        subtitle: " - событийно - оринтированный сервис для тебя ❤️",
        whatWeDo: `Бывает такое, что хочется куда - нибудь сходить, но совсем неясно куда... Интерактивная карта состоит из меток настроения, окрашенных с соответствующий цвет. Яркие цвета означают хорошее настроение, Тёмные, напротив, грустное и депрессивное. Ориентируясь по цветовым меткам можно найти место, где сейчас хорошо и пойти именно туда.`,
        businessValue: "На карте также находятся иконки заведений. Пользователи могут пойти в те места, рядом с которыми находятся счастливые люди и провести там время. Сервис позволяет заведениям обеспечить хороший поток посетителей и контролировать удовлетворённость посетителей своим продуктом.",
        date: "20.03.2019",
        imgSrc: "moodcolor.png",
        technologies: "Spring Boot, Mongo, Docker, GCP",
        presentation: "https://firebasestorage.googleapis.com/v0/b/portfolio-24452.appspot.com/o/MUDKALAR.pdf?alt=media",
        links: [
          {
            textLabel: "Backend",
            url: "https://gitlab.com/r_terekhov/mood-color-backend"
          },
          {
            textLabel: "Цвет настроения в Play Market",
            url: "https://play.google.com/store/apps/details?id=ru.nsu.startup.mood_color&hl=ru"
          }
        ]
      },
  ]
  },
  mutations: {
  },
  actions: {
   
  },
  modules: {
  },
  getters:{
    getProjectById: (state) => (id) => {
      return state.projects.find(project => project.id == id)
    }
  }
})
